<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Playlist_Song;

class PlaylistSongController extends Controller
{
    public function displaySongs(){
        return DB::table('playlists_songs')->get();
    }
    
    public function zPlaylistSongs(Request $request){

        $newPlaylistSong = new Playlist_Song();
        $newPlaylistSong->song_id = $request->song_id;
        $newPlaylistSong->playlist_id = $request->playlist_id;
        $newPlaylistSong->save();
        return $newPlaylistSong;
}
}