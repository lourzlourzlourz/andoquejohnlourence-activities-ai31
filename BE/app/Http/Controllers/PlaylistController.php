<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Playlist;

class PlaylistController extends Controller
{
    public function displayPlaylists(){
        return DB::table('playlists')->get();
    }
    

    public function zPlaylist(Request $request){

        $newPlaylist = new Playlist();
        $newPlaylist->name = $request->name;
        $newPlaylist->save();
        return response() -> json (['message'=>'Playlist added successful'], 200);
}
}