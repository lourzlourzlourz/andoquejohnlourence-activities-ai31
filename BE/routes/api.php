<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SongController;
use App\Http\Controllers\PlaylistController;
use App\Http\Controllers\PlaylistSongController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
//Songs 
Route::get('/songs', [SongController::class, 'displaySongs']);
Route::post('/upload', [SongController::class, 'zSongs']);
//Playlist 
Route::get('/playlists', [PlaylistController::class, 'displayPlaylists']);
Route::post('/create', [PlaylistController::class, 'zPlaylist']);
//PlaylistSong 
Route::get('/playlistsongs', [PlaylistSongController::class, 'displayPlaylistSongs']);
Route::post('/creates', [PlaylistSongController::class, 'zPlaylistSong']);
