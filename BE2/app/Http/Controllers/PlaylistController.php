<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Playlist;

class PlaylistController extends Controller
{
    public function displaySongs(){
        return DB::table('playlists')->get();
    }
    

    public function store(Request $request){

        $newPlaylist = new Playlist();
        $newPlaylist->name = $request->title;
        $newPlaylist>save();
        return $newPlaylist;
}
}