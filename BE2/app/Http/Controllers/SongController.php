<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Songs;

class SongController extends Controller

{
    public function displaySongs(){
        return DB::table('songs')->get();
    }

    //Upload
    public function store(Request $request){

        $newSong = new Songs();
        $newSong->title = $request->title;
        $newSong->length = $request->length;
        $newSong->artist = $request->artist;
        $newSong->save();
        return $newSong;
}
}